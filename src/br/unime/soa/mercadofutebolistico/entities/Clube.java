package br.unime.soa.mercadofutebolistico.entities;

import java.util.ArrayList;
import java.util.List;

public class Clube {
	
	private List<Jogador> jogadores;
	private String nome;
	
	public Clube(String nome) {
		this.nome = nome;
		jogadores = new ArrayList<Jogador>();
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}

	public Clube(List<Jogador> jogadores) {
		this.jogadores = jogadores;
	}

	public List<Jogador> getJogadores() {
		return jogadores;
	}

	public void setJogadores(List<Jogador> jogadores) {
		this.jogadores = jogadores;
	}
	
	public boolean addJogador(Jogador jogador){
		
		try {
			for (Jogador j : jogadores) {
				if (j.getNome() == jogador.getNome()) {
					return false;
				}			
			}
		} catch (NullPointerException e) {
			return false;
		}
		
		jogadores.add(jogador);
		
		return true;
		
	}
}
