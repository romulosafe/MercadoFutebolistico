package br.unime.soa.mercadofutebolistico.services;

import java.util.ArrayList;
import java.util.List;

import br.unime.soa.mercadofutebolistico.entities.Clube;
import br.unime.soa.mercadofutebolistico.entities.Jogador;

/** Servi�o de Mercado de Jogadores
 * @author R�mulo Ferreira
 * @since 19/10/2016
 */

public class Mercado {
	
	static List<Clube> clubes ;
	
	public Mercado() {
		if (null == clubes) {
			clubes = new ArrayList<>();
		}
	}
	
	/** M�todo para listagem de jogadores ofertados no mercado
	 * @author R�mulo Ferreira
	 * @return List<String> - Lista de nomes de jogadores em xml
	 */
	public String listarJogadores(){
		
		return jogadoresXML();
	}
	
	/** M�todo para inserir novo jogador ou atualizar clube
	 * @author R�mulo Ferreira
	 * @param jogador String - Nome do jogador a ser inserido no mercado
	 * @param clube String - Nome do clube que o jogador pertence
	 * @return String - Valor par�o "ok" para opera��o realizada com sucesso 
	 */
	public String salvarJogador(String jogador,String clube){

		if (!validarJogador(jogador, clube)) {
			return "Os par�metros n�o podem ser vazios";
		}		
		
		if(existeJogador(jogador)){
			trocaJogador(jogador,clube);
			return "ok";
		}
		
		insereJogador(jogador,clube);
		
		return "ok";
	}
	

	private void insereJogador(String jogador, String clube) {
		
		Jogador novoJogador = new Jogador(jogador);
		
		for (int i = 0; i < clubes.size(); i++) {
			if(clubes.get(i).getNome().equalsIgnoreCase(clube)){
				clubes.get(i).addJogador(novoJogador);
				return;
			}
		}
		
		Clube novoClube = new Clube(clube);
		novoClube.addJogador(novoJogador);
		clubes.add(novoClube);
	}

	private void trocaJogador(String jogador, String clube) {
		List<Jogador> jogadores = null;
		
		for (int i = 0; i < clubes.size(); i++) {
			jogadores = clubes.get(i).getJogadores();

			for (int y = 0; y < jogadores.size(); y++) {
				if (jogadores.get(y).getNome().equalsIgnoreCase(jogador)) {
					jogadores.remove(y);
					clubes.get(i).setJogadores(jogadores);
					insereJogador(jogador, clube);
				}
			}
		}
	}
	
	private boolean existeJogador(String nomeDoJogador) {
		
		for (Clube clube : clubes) {
			for (Jogador j : clube.getJogadores()) {
				if (j.getNome().equalsIgnoreCase(nomeDoJogador)) {
					return true;
				}
			}
		}
		return false;
	}

	/** M�todo para serializar a lista de jogadores em XML
	 * @author R�mulo Ferreira
	 * @param jogador String - Nome do jogador a ser inserido no mercado
	 * @param clube String - Nome do clube que o jogador pertence
	 * @return boolean - true se o jogador for v�lido caso contr�rio false
	 */
	private boolean validarJogador(String jogador, String clube) {
		 return !(jogador == null || clube == null );
	}
	
	/** M�todo para serializar a lista de jogadores em XML
	 * @author R�mulo Ferreira
	 * @return String - Lista de jogadores em XML
	 */
	public String jogadoresXML(){
		
		String XML = "";
		List<Jogador> jogadores;
		
		for (Clube clube : clubes) {
			XML += "<clube nome=\""+clube.getNome()+"\">";
			jogadores = clube.getJogadores();
			
			for (Jogador j : jogadores) {
				XML += "<jogador>"+j.getNome()+"</jogador>";
			}
			
			XML += "</clube>";
		}
		
		
		XML = "<clubes>"+ XML +"</clubes>";
		
		return XML;
	} 
}